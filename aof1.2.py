from __future__ import print_function
import sys
import itertools


def main():
    items = []
    for line in sys.stdin:
        items.append(int(line))

    for pair in itertools.combinations(items, 2):
        if sum(pair) == 2020:
            print(pair[0] * pair[1])


if __name__ == '__main__':
    main()
