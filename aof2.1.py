from __future__ import print_function
import sys


def main():
    items = []
    for line in sys.stdin:
        split = line.split()
        val_range = split[0].split("-")

        items.append(
            {
                "min": int(val_range[0]),
                'max': int(val_range[1]),
                'key': split[1].strip(":"),
                'password': split[2],
            }
        )

    count = 0
    for item in items:
        x = item['password'].count(item['key'])
        if item['min'] <= x and x <= item['max']:
            count += 1

    print(count)


if __name__ == '__main__':
    main()
