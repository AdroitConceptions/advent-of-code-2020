from __future__ import print_function
import sys


def main():
    items = []
    for line in sys.stdin:
        split = line.split()
        val_range = split[0].split("-")

        items.append(
            {
                "positions": [int(x)-1 for x in val_range],
                'key': split[1].strip(":"),
                'password': split[2],
            }
        )

    count = 0
    for item in items:
        if sum([item['password'][x] == item['key'] for x in item["positions"]]) == 1:
            count += 1

    print(count)


if __name__ == '__main__':
    main()
