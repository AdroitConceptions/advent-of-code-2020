from __future__ import print_function
import sys


def internal_bags(bag_type, data):
    if len(data[bag_type]) == 0:
        print("empty")
        return 1
    contains = 1
    for x in data[bag_type]:
        print(x)
        contains += int(x[0]) * internal_bags(x[1], data)
    return contains


def main():
    contains = {}
    for line in sys.stdin:
        x = line.split(" bags contain ")
        y = x[1].strip().split(',')
        y = [w.strip(" .\n") for w in y]

        if 'no other bags' in y:
            contains[x[0]] = []
        else:
            contains[x[0]] = [[w.split()[0], ' '.join(w.split()[1:3])]
                              for w in y]

    print(internal_bags('shiny gold', contains) - 1)


if __name__ == "__main__":
    main()
