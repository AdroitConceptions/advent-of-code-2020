from __future__ import print_function
import sys
from functools import reduce


def read_data():
    data = []

    for line in sys.stdin:
        data.append(int(line.strip()))

    data.append(0)
    data.append(max(data)+3)
    return sorted(data)

# recursion method (really slow on full data set)


def paths_to_end(starting, data):
    if len(data) == 1:
        return 1

    paths = 0
    for i in range(min(3, len(data))):
        if data[i] - starting < 4:
            paths += paths_to_end(data[i], data[i+1:])

    return paths

# chain of possible paths method


def break_at_3_jump(data):
    results = []
    last_break = 0
    for i in range(1, len(data)):
        if data[i] - data[i-1] == 3:
            results.append(data[last_break:i])
            last_break = i
    # print(results)
    return results


def main():
    data = read_data()

    diff_1_cnt = 0
    diff_3_cnt = 0
    for i in range(len(data) - 1):
        diff = data[i+1] - data[i]
        if diff == 1:
            diff_1_cnt += 1
        if diff == 3:
            diff_3_cnt += 1

    print("1: " + str(diff_1_cnt))
    print("3: " + str(diff_3_cnt))
    print(diff_1_cnt * diff_3_cnt)

    # print("paths: " + str(paths_to_end(data[0], data[1:])))

    split_data = break_at_3_jump(data)
    # this call to paths_to_end isn't using data as expected, but is getting the correct result
    # ¯\_(ツ)_/¯
    hops = [1 if len(x) <= 2 else paths_to_end(x[0], x)
            for x
            in split_data]
    # print(hops)
    print(reduce(lambda x, y: x * y, hops))


if __name__ == '__main__':
    main()
