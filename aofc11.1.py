from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append([x for x in line.strip()])

    return data


def adjacent_count(data, x, y):
    adjacent_cnt = 0
    for y_step in range(max(0, y-1), min(len(data), y+2)):
        for x_step in range(max(0, x-1), min(len(data[0]), x+2)):
            if x == x_step and y == y_step:
                continue
            if data[y_step][x_step] == 'H':
                adjacent_cnt += 1

    return adjacent_cnt


def step(data):
    changed = False

    new_data = []
    for y in range(len(data)):
        new_row = []
        for x in range(len(data[0])):
            adjacent_cnt = adjacent_count(data, x, y)
            if adjacent_cnt == 0 and data[y][x] == 'L':
                changed = True
                new_row.append('H')
            elif adjacent_cnt >= 4 and data[y][x] == 'H':
                changed = True
                new_row.append('L')
            else:
                new_row.append(data[y][x])
        new_data.append(new_row)

    return changed, new_data


def count_occupied(data):
    occupied = 0
    for y in range(len(data)):
        for x in range(len(data[0])):
            if data[y][x] == 'H':
                occupied += 1
    return occupied


def print_data(data):
    for row in data:
        print("".join(row))
    print()


def main():
    data = read_data()

    changed = True
    while changed:
        changed, data = step(data)

    print(count_occupied(data))


if __name__ == '__main__':
    main()
