from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append([x for x in line.strip()])

    return data


DIRECTIONS = [
    [-1, -1],
    [-1, 0],
    [-1, 1],
    [0, -1],
    [0, 1],
    [1, -1],
    [1, 0],
    [1, 1],
]


def see_count(data, x, y):
    see_cnt = 0
    for direction in DIRECTIONS:
        found_chair = False
        distance = 0
        while not found_chair:
            distance += 1
            test_x = x + distance * direction[1]
            test_y = y + distance * direction[0]
            if test_x < 0 or test_x >= len(data[0]) or test_y < 0 or test_y >= len(data):
                found_chair = True
            else:
                in_spot = data[test_y][test_x]

                if in_spot == "H":
                    see_cnt += 1
                if in_spot in ["H", "L"]:
                    found_chair = True

    return see_cnt


def step(data):
    changed = False

    new_data = []
    for y in range(len(data)):
        new_row = []
        for x in range(len(data[0])):
            adjacent_cnt = see_count(data, x, y)
            if adjacent_cnt == 0 and data[y][x] == 'L':
                changed = True
                new_row.append('H')
            elif adjacent_cnt >= 5 and data[y][x] == 'H':
                changed = True
                new_row.append('L')
            else:
                new_row.append(data[y][x])
        new_data.append(new_row)

    return changed, new_data


def count_occupied(data):
    occupied = 0
    for y in range(len(data)):
        for x in range(len(data[0])):
            if data[y][x] == 'H':
                occupied += 1
    return occupied


def print_data(data):
    for row in data:
        print("".join(row))
    print()


def main():
    data = read_data()

    changed = True
    steps = 0
    while changed:
        steps += 1
        changed, data = step(data)

    print(count_occupied(data))


if __name__ == '__main__':
    main()
