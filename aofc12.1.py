from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


FACINGS = ['N', 'E', 'S', 'W']


def main():
    data = read_data()

    current_facing = 1
    position = [0, 0]

    for item in data:
        action = item[0]
        value = int(item[1:])
        if action in ['R', 'L']:
            rotation_amount = value / 90
            current_facing += rotation_amount if action == 'R' else -rotation_amount
            current_facing %= 4
        if action == 'F':
            action = FACINGS[current_facing]

        if action == 'N':
            position[1] -= value
        if action == 'S':
            position[1] += value
        if action == 'W':
            position[0] -= value
        if action == 'E':
            position[0] += value

        print(FACINGS[current_facing], position)

    print(sum(position))


if __name__ == '__main__':
    main()
