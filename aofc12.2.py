from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


FACINGS = ['N', 'E', 'S', 'W']


def rotate_waypoint(waypoint, direction, value):
    # waypoint is [x, y]
    # N is +
    # E is +
    if direction == 'R':
        if value == 1:
            return [waypoint[1], -waypoint[0]]
        if value == 2:
            return [-waypoint[0], -waypoint[1]]
        if value == 3:
            return [-waypoint[1], waypoint[0]]
    else:
        if value == 1:
            return [-waypoint[1], waypoint[0]]
        if value == 2:
            return [-waypoint[0], -waypoint[1]]
        if value == 3:
            return [waypoint[1], -waypoint[0]]
    return waypoint


def main():
    data = read_data()

    position = [0, 0]
    waypoint = [10, 1]

    for item in data:
        action = item[0]
        value = int(item[1:])
        if action in ['R', 'L']:
            rotation_amount = value / 90
            waypoint = rotate_waypoint(waypoint, action, rotation_amount)
        if action == 'F':
            position[0] += waypoint[0] * value
            position[1] += waypoint[1] * value

        if action == 'N':
            waypoint[1] += value
        if action == 'S':
            waypoint[1] -= value
        if action == 'W':
            waypoint[0] -= value
        if action == 'E':
            waypoint[0] += value

        print(position, waypoint)

    print(abs(position[0]) + abs(position[1]))


if __name__ == '__main__':
    main()
