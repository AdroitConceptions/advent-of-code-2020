from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def main():
    data = read_data()

    earliest = int(data[0])
    buses = [int(x) for x in data[1].split(',') if x != 'x']

    next_departures = [x * int(earliest/x) + x - earliest for x in buses]
    print(earliest)
    print(buses)
    print(next_departures)
    i = next_departures.index(min(next_departures))
    print(next_departures[i], buses[i])
    print(next_departures[i] * buses[i])


if __name__ == '__main__':
    main()
