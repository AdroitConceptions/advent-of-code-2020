from __future__ import print_function
import sys
from functools import reduce


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def chinese_remainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a*b, n)
    for n_i, a_i in zip(n, a):
        p = prod/n_i
        sum += a_i * mul_inv(p, n_i)*p
    return sum % prod


def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1:
        return 1
    while a > 1:
        q = a // b
        a, b = b, a % b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0:
        x1 += b0
    return x1


def crt(l):
    x = reduce(lambda a, b: a*b[0], l, 1)
    w = sum(x/a*b*pow(x/a, a-2, a)for a, b in l)
    M = reduce(lambda x, y: x * y, [a[0] for a in l])

    print(x, w, M)
    print(w % M)


def main():
    data = read_data()

    buses = [int(x) if x != 'x' else x for x in data[1].split(',')]

    buses = [(x, i) for i, x in enumerate(buses) if x != 'x']

    cr = chinese_remainder([x[0] for x in buses], [x[0] - x[1] for x in buses])
    print(cr)
    for x in buses:
        print(x, cr % x[0])


if __name__ == '__main__':
    main()
