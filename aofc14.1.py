from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def main():
    data = read_data()

    mask_1 = 0
    mask_0 = 0

    memory_locations = {}

    for item in data:
        if 'mask' in item:
            mask = item.split(' ')[2]
            mask_1 = int(mask.replace('X', '0'), 2)
            mask_0 = int(mask.replace('X', '1'), 2)

        else:
            split_1 = item.split(' ')
            mem_loc = int(split_1[0].split('[')[1].split(']')[0])
            value = int(split_1[2])

            value |= mask_1
            value &= mask_0

            memory_locations[mem_loc] = value

    sum = 0
    for key, value in memory_locations.items():
        sum += value

    print(sum)


if __name__ == '__main__':
    main()
