from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def get_all_masked_locs(mask, mem_loc):
    if len(mask) == 0:
        return [""]
    if mask[0] == "0":
        return [mem_loc[0] + x for x in get_all_masked_locs(mask[1:], mem_loc[1:])]
    if mask[0] == "1":
        return ["1" + x for x in get_all_masked_locs(mask[1:], mem_loc[1:])]
    sub = get_all_masked_locs(mask[1:], mem_loc[1:])
    return ["0" + x for x in sub] + ["1" + x for x in sub]


def main():
    data = read_data()

    mask = ''

    memory_locations = {}

    for item in data:
        if 'mask' in item:
            mask = item.split(' ')[2]

        else:
            split_1 = item.split(' ')
            mem_loc = format(
                int(split_1[0].split('[')[1].split(']')[0]), "036b")
            value = int(split_1[2])

            for masked_loc in get_all_masked_locs(mask, mem_loc):
                memory_locations[int(masked_loc)] = value

    sum = 0
    for value in memory_locations.values():
        sum += value

    print(sum)


if __name__ == '__main__':
    main()
