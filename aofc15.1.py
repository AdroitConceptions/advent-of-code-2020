from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def main():
    data = [int(x) for x in read_data()[0].split(',')]

    data.reverse()
    while len(data) < 2020:
        last_spoken = data[0]

        try:
            prior_index = data.index(last_spoken, 1)
        except ValueError:
            prior_index = 0
        # print(prior_index)
        data.insert(0, prior_index)

    print(data[0])


if __name__ == '__main__':
    main()
