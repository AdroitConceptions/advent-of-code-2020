from __future__ import print_function
import sys
import time


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def main():
    data = [int(x) for x in read_data()[0].split(',')]
    start_time = time.time()

    last_spoken_times = {x: i for i, x in enumerate(data[:-1])}

    print(last_spoken_times)
    last_spoken = data[-1]

    for current_count in range(len(data)-1, 30000000-1):
        if current_count % 30000 == 0:
            print(current_count, time.time() - start_time)

        # check if last_spoken has been spoken before
        lst = last_spoken_times.get(last_spoken, -1)
        if lst == -1:
            next_spoken = 0
        else:
            next_spoken = current_count - lst

        #print(current_count, last_spoken, next_spoken)

        last_spoken_times[last_spoken] = current_count
        last_spoken = next_spoken

    # print(last_spoken_times)
    print(last_spoken)


if __name__ == '__main__':
    main()
