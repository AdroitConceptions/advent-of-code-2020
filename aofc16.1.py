from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def valid_for_any_rule(value, rules):
    for rule in rules:
        for r in rule[1]:
            if r[0] <= value and value <= r[1]:
                return True
    return False


def main():
    data = read_data()

    rules = []
    your_ticket = None
    nearby_tickets = []
    step = 0
    for row in data:
        if row == "":
            step += 1
            continue
        if "ticket" in row:
            continue
        if step == 0:
            split1 = row.split(":")
            split2 = split1[1].strip().split(" or ")
            rules.append([split1[0], [[int(y) for y in x.split('-')]
                                      for x in split2]])
        if step == 1:
            your_ticket = [int(x) for x in row.split(',')]
        if step == 2:
            nearby_tickets.append([int(x) for x in row.split(',')])

    error_sum = 0
    for ticket in nearby_tickets:
        for value in ticket:
            if not valid_for_any_rule(value, rules):
                error_sum += value

    print(error_sum)


if __name__ == '__main__':
    main()
