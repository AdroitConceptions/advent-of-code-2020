from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def valid_for_any_rule(value, rules):
    for rule in rules:
        for r in rule[1]:
            if r[0] <= value and value <= r[1]:
                return True
    return False


def valid_for_rule(value, rule):
    for r in rule[1]:
        if r[0] <= value and value <= r[1]:
            return True
    return False


def main():
    data = read_data()

    rules = []
    your_ticket = None
    nearby_tickets = []
    step = 0
    for row in data:
        if row == "":
            step += 1
            continue
        if "ticket" in row:
            continue
        if step == 0:
            split1 = row.split(":")
            split2 = split1[1].strip().split(" or ")
            rules.append([split1[0], [[int(y) for y in x.split('-')]
                                      for x in split2]])
        if step == 1:
            your_ticket = [int(x) for x in row.split(',')]
        if step == 2:
            nearby_tickets.append([int(x) for x in row.split(',')])

    valid_tickets = []
    for ticket in nearby_tickets:
        ticket_valid = True
        for value in ticket:
            if not valid_for_any_rule(value, rules):
                ticket_valid = False
        if ticket_valid:
            valid_tickets.append(ticket)

    # a this point we have a list of valid tickets
    # now to determin what columns each rule could be for

    rule_valid_for = [range(len(your_ticket)) for x in range(len(rules))]
    for ticket in valid_tickets:
        for i, value in enumerate(ticket):
            for j, rule in enumerate(rules):
                if not valid_for_rule(value, rule):
                    if i in rule_valid_for[j]:
                        rule_valid_for[j].remove(i)

    cleaning_up = True
    while cleaning_up:
        cleaning_up = False
        for source_rule in rule_valid_for:
            for target_rule in rule_valid_for:
                if source_rule == target_rule:
                    continue
                if len(source_rule) == 1:
                    x = source_rule[0]
                    if x in target_rule:
                        target_rule.remove(x)
                        cleaning_up = True

    m = 1
    for i, rule in enumerate(rules):
        # print(rule)
        if "departure" in rule[0]:
            #   print("\t" + str(rule_valid_for[i][0]))
            m *= your_ticket[rule_valid_for[i][0]]

    print(m)


if __name__ == '__main__':
    main()
