from __future__ import print_function
import sys

BASE = [-1, 0, 1]

DIRECTIONS = [
    (x, y, z)
    for x in BASE
    for y in BASE
    for z in BASE
    if x != 0 or y != 0 or z != 0
]


def should_be_active(grid_loc, grid):
    neibor_cnt = 0
    for direction in DIRECTIONS:
        test_loc = (
            grid_loc[0] + direction[0],
            grid_loc[1] + direction[1],
            grid_loc[2] + direction[2])
        if grid.get(test_loc, False):
            neibor_cnt += 1
    if neibor_cnt == 3:
        return True
    if neibor_cnt == 2:
        return grid.get(grid_loc, False)
    return False


def step(grid_in):
    grid_out = {}
    for x in range(-6, 14):
        for y in range(-6, 14):
            for z in range(-6, 7):
                grid_loc = (x, y, z)
                if should_be_active(grid_loc, grid_in):
                    grid_out[grid_loc] = True

    return grid_out


def read_data():
    data = []

    for line in sys.stdin:
        data.append([x for x in line.strip()])

    return data


def main():
    data = read_data()

    grid = {}
    for i, row in enumerate(data):
        for j, item in enumerate(row):
            if item == '#':
                grid[(i, j, 0)] = True

    for i in range(6):
        grid = step(grid)

    print(len(grid))


if __name__ == '__main__':
    main()
