from __future__ import print_function
import sys

BASE = [-1, 0, 1]

DIRECTIONS = [
    (x, y, z, w)
    for x in BASE
    for y in BASE
    for z in BASE
    for w in BASE
    if x != 0 or y != 0 or z != 0 or w != 0
]


def should_be_active(grid_loc, grid):
    neibor_cnt = 0
    for direction in DIRECTIONS:
        test_loc = (
            grid_loc[0] + direction[0],
            grid_loc[1] + direction[1],
            grid_loc[2] + direction[2],
            grid_loc[3] + direction[3],
        )
        if grid.get(test_loc, False):
            neibor_cnt += 1
    if neibor_cnt == 3:
        return True
    if neibor_cnt == 2:
        return grid.get(grid_loc, False)
    return False

# an improvment here would be to use a count dictionary
# loop over every item in the current grid, add 1 to the adjacent directions
# in the count dictionary
# the loop over the count dictionary, if 3 set the result node to True
# if 2, set to same state as old grid location
# by only keeping the trues in, this should be more performant - at least if the grid that
# is active is relatively sparse and would support endless steps
# where the below can't handle more then 6 steps, as the range being itterated over
# would/could have issues after 6 steps


def step(grid_in):
    grid_out = {}
    for x in range(-6, 14):
        for y in range(-6, 14):
            for z in range(-6, 7):
                for w in range(-6, 7):
                    grid_loc = (x, y, z, w)
                    if should_be_active(grid_loc, grid_in):
                        grid_out[grid_loc] = True

    return grid_out


def read_data():
    data = []

    for line in sys.stdin:
        data.append([x for x in line.strip()])

    return data


def main():
    data = read_data()

    grid = {}
    for i, row in enumerate(data):
        for j, item in enumerate(row):
            if item == '#':
                grid[(i, j, 0, 0)] = True

    for i in range(6):
        grid = step(grid)

    print(len(grid))


if __name__ == '__main__':
    main()
