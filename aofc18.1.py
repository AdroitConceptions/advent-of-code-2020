from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def solv(expression):
    # print(expression)
    # resolve parens
    while "(" in expression:
        start = expression.find("(")
        end = 0
        depth = 0
        for i in range(start, len(expression)):
            if expression[i] == '(':
                depth += 1
            if expression[i] == ')':
                depth -= 1
            if depth == 0:
                end = i
                break
        sub_val = solv(expression[start + 1:end])
        expression = expression[:start] + str(sub_val) + expression[end + 1:]
        # print(expression)

    # solv expression left to right
    items = expression.split()
    while len(items) > 1:
        val = eval("".join(items[:3]))
        items = [str(val)] + items[3:]
    return items[0]


def main():
    data = read_data()

    sum = 0
    for row in data:
        sum += int(solv(row))

    print(sum)


if __name__ == '__main__':
    main()
