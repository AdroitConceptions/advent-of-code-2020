from __future__ import print_function
import sys
import re


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def build_re_from_dict_helper(rules, value_string):
    x = ""
    for i in value_string.strip().split(" "):
        x += build_re_exp_from_dict(rules, i.strip())
    return x


def build_re_exp_from_dict(rules, index):
    item = rules[index]
    if '"' in item:
        return item[1]

    # replacements
    if index == "8":
        return "(?:" + build_re_from_dict_helper(rules, "42") + ")+"

    # there has to be a better way to do this then keep adding more 'options' until the count stops going up
    if index == "11":
        x42 = build_re_from_dict_helper(rules, "42")
        x31 = build_re_from_dict_helper(rules, "31")
        return "(?:(?:" + x42 + x31 \
            + ")|(?:" + x42 + x42 + x31 + x31 \
            + ")|(?:" + x42 + x42 + x42 + x31 + x31 + x31 \
            + ")|(?:" + x42 + x42 + x42 + x42 + x31 + x31 + x31 + x31 \
            + ")|(?:" + x42 + x42 + x42 + x42 + x42 + x31 + x31 + x31 + x31 + x31 \
            + "))"

    if '|' in item:
        item_split = item.split("|")
        x = []
        for i_s in item_split:
            x.append(build_re_from_dict_helper(rules, i_s))
        return "(?:" + "|".join(x) + ")"
    else:
        return build_re_from_dict_helper(rules, item)


def build_re_expresion(rules):
    rule_dict = {}
    for rule in rules:
        s = rule.split(":")
        rule_dict[s[0]] = s[1].strip()

    return "^" + build_re_exp_from_dict(rule_dict, "0") + "$"


def main():
    data = read_data()

    x = data.index("")
    rules = data[:x]
    data = data[x+1:]

    re_exp = build_re_expresion(rules)
    regex = re.compile(re_exp)

    valid_count = 0
    for item in data:
        if regex.search(item):
            valid_count += 1

    print(valid_count)


if __name__ == '__main__':
    main()
