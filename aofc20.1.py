from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def convert_to_number(value):
    value = value.replace("#", "1").replace(".", "0")
    return int(value, 2)


# tile side values will be entered clockwise, that way, the value
# works for any rotation, just the order of values would change


def create_tile(tile_name, tile_rows):
    tile = {'name': int(tile_name), 'sides': [], 'fliped': []}
    tile['sides'].append(convert_to_number(tile_rows[0]))
    tile['sides'].append(convert_to_number(
        ''.join([x[-1] for x in tile_rows])))
    tile['sides'].append(convert_to_number(tile_rows[-1][::-1]))
    tile['sides'].append(convert_to_number(
        ''.join([x[0] for x in tile_rows[::-1]])))

    tile['fliped'].append(convert_to_number(tile_rows[0][::-1]))
    tile['fliped'].append(convert_to_number(
        ''.join([x[-1] for x in tile_rows[::-1]])))
    tile['fliped'].append(convert_to_number(tile_rows[-1]))
    tile['fliped'].append(convert_to_number(
        ''.join([x[0] for x in tile_rows])))
    return tile


def conver_data_to_tiles(data):
    tiles = []

    tile_name = None
    tile_rows = []
    for row in data:
        if 'Tile' in row:
            if tile_name is not None:
                tiles.append(create_tile(tile_name, tile_rows))
            tile_name = row.split()[1].strip(":")
            tile_rows = []
        elif len(row) == 0:
            continue
        else:
            tile_rows.append(row)

    tiles.append(create_tile(tile_name, tile_rows))
    return tiles


def main():
    data = read_data()
    tiles = conver_data_to_tiles(data)

    sides_count = {}
    for tile in tiles:
        for side in tile['sides']:
            sides_count[side] = sides_count.get(side, 0) + 1
        for side in tile['fliped']:
            sides_count[side] = sides_count.get(side, 0) + 1

    mult = 1
    for tile in tiles:
        matches = sum([1 if sides_count[x] == 2 else 0 for x in tile['sides']])
        if matches == 2:
            print(tile['name'], matches)
            mult *= tile['name']

    print(mult)


if __name__ == '__main__':
    main()
