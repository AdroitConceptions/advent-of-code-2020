from __future__ import print_function
import sys
import math
from copy import deepcopy
import re

SEA_MONSTER = """                 # 
#    ##    ##    ###
 #  #  #  #  #  #   """


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def convert_to_number(value):
    value = value.replace("#", "1").replace(".", "0")
    return int(value, 2)


# tile side values will be entered clockwise, that way, the value
# works for any rotation, just the order of values would change


def create_tile(tile_name, tile_rows):
    tile = {'name': int(tile_name), 'sides': [], 'rows': tile_rows}
    tile['sides'].append(convert_to_number(tile_rows[0]))
    tile['sides'].append(convert_to_number(
        ''.join([x[-1] for x in tile_rows])))
    tile['sides'].append(convert_to_number(tile_rows[-1][::-1]))
    tile['sides'].append(convert_to_number(
        ''.join([x[0] for x in tile_rows[::-1]])))

    tile['sides'].append(convert_to_number(tile_rows[0][::-1]))
    tile['sides'].append(convert_to_number(
        ''.join([x[-1] for x in tile_rows[::-1]])))
    tile['sides'].append(convert_to_number(tile_rows[-1]))
    tile['sides'].append(convert_to_number(
        ''.join([x[0] for x in tile_rows])))
    return tile


def conver_data_to_tiles(data):
    tiles = []

    tile_name = None
    tile_rows = []
    for row in data:
        if 'Tile' in row:
            if tile_name is not None:
                tiles.append(create_tile(tile_name, tile_rows))
            tile_name = row.split()[1].strip(":")
            tile_rows = []
        elif len(row) == 0:
            continue
        else:
            tile_rows.append(row)

    tiles.append(create_tile(tile_name, tile_rows))
    return tiles


def determine_tile_types(tiles):
    sides_count = {}
    for tile in tiles:
        for side in tile['sides']:
            sides_count[side] = sides_count.get(side, 0) + 1

    corner_tiles = []
    side_tiles = []
    middle_tiles = []

    for tile in tiles:
        matches = sum(
            [1 if sides_count[x] == 2 else 0 for x in tile['sides'][0:4]])
        tile['matches'] = [sides_count[x] == 2 for x in tile['sides'][0:4]]
        if matches == 2:
            corner_tiles.append(tile)
        if matches == 3:
            side_tiles.append(tile)
        if matches == 4:
            middle_tiles.append(tile)

    # print(sorted(sides_count.keys()))
    # print(sides_count[710])
    # print(710 in sides_count.keys())
    return corner_tiles + side_tiles + middle_tiles


def rotate_tile(tile):
    # clockwise rotation
    # print("rotating Tile")
    # print(tile['sides'])
    # print(tile['matches'])
    tile['sides'] = [tile['sides'].pop(3)] + tile['sides']
    tile['matches'] = [tile['matches'].pop(3)] + tile['matches']
    tile['rows'] = [''.join(x) for x in zip(*tile['rows'][::-1])]
    # print(tile['sides'])
    # print(tile['matches'])
    return tile


def flip_tile(tile):
    # flip around x axis
    # print("flipping")
    tile['matches'] = [tile['matches'][x] for x in [2, 1, 0, 3]]
    tile['rows'] = tile['rows'][::-1]
    tile['was_fliped'] = True
    return tile


def rotate_first_tile(tile):
    # print("rotate first")
    # up, right, down, left
    while tile['matches'] != [False, True, True, False]:
        tile = rotate_tile(tile)
    return tile


def matching_value(tile, position):
    if position == 0:
        return convert_to_number(tile['rows'][0][::-1])
    if position == 3:
        return convert_to_number(''.join([x[0] for x in tile['rows']]))

    raise Exception("not yet implemented: {}".format(position))


def print_a_tile(tile):
    bob = ""
    for r in tile['rows']:
        bob += r + "\n"
    print(bob)


def rotate_flip_tile(tile, value, position):
    for i in range(9):
        if matching_value(tile, position) == value:
            return tile
        if i == 4:
            tile = flip_tile(tile)
        else:
            tile = rotate_tile(tile)
    raise Exception("didn't find correct orientation")


def find_tile_with_value(tiles, value):
    found = []
    for tile in tiles:
        if value in tile['sides']:
            found.append(tile)
    if len(found) > 0:
        if(len(found)) > 1:
            print(len(found))
        return found[0]
    raise Exception("Tile with Value: {} not found".format(value))


def print_row(row):
    for i in range(0, 11):
        line = ""
        for item in row:
            if item is None:
                continue
            if i == 10:
                line += "           "
            else:
                line += item['rows'][i] + ' '

        print(line)


def print_row2(row):
    for i in range(0, 4):
        line = ""
        for item in row:
            if item is None:
                continue
            if i == 0:
                line += "{}  {}{:3d}    ".format(
                    "X" if item.get('was_fliped', False) else " ",
                    "T" if item['matches'][0] else "F",
                    item['sides'][0]
                )
            if i == 1:
                line += "{}{:3d} {}{:3d}  ".format(
                    "T" if item['matches'][3] else "F",
                    item['sides'][3],
                    "T" if item['matches'][1] else "F",
                    item['sides'][1]
                )
            if i == 2:
                line += "   {}{:3d}    ".format(
                    "T" if item['matches'][2] else "F",
                    item['sides'][2]
                )
        print(line)


def create_base_map(tiles):
    side_len = int(math.sqrt(len(tiles)))

    grid = [[None for y in range(side_len)] for x in range(side_len)]
    for x in range(len(grid)):
        try:
            for y in range(len(grid[0])):
                if y == 0:
                    if x == 0:
                        tile = tiles.pop(0)
                        grid[x][y] = rotate_first_tile(tile)
                        # grid[x][y] = 1
                    else:
                        # look up
                        value = convert_to_number(
                            grid[x-1][y]['rows'][-1][::-1])
                        tile = find_tile_with_value(tiles, value)
                        tiles.remove(tile)
                        grid[x][y] = rotate_flip_tile(tile, value, 0)
                        # grid[x][y] = 3
                else:
                    # look left
                    value = convert_to_number(
                        ''.join([w[-1] for w in grid[x][y-1]['rows']]))
                    tile = find_tile_with_value(tiles, value)
                    tiles.remove(tile)
                    grid[x][y] = rotate_flip_tile(tile, value, 3)
            # print_row(grid[x])
        except Exception as ex:
            print_row(grid[x])
            print("xxxxxx")
            raise ex

    return grid


def test_flip():
    tile = create_tile(
        1,
        [
            "###..",
            ".###.",
            "#...#",
            "####.",
            ".#.#.",
        ]
    )
    tile['matches'] = [False, True, True, False]

    print(tile['sides'], tile['matches'])
    for row in tile['rows']:
        print(row)
    tile = rotate_tile(tile)
    print(tile['sides'],  tile['matches'])
    for row in tile['rows']:
        print(row)
    tile = create_tile(
        1, tile['rows']
    )
    tile['matches'] = [False, True, True, False]
    print(tile['sides'], tile['matches'])
    for row in tile['rows']:
        print(row)


def find_final_orientation(final, regex):
    for i in range(8):
        match = regex.search(''.join(final))
        if match:
            return final
        if i == 3:
            final = final[::-1]
        else:
            final = [''.join(x) for x in zip(*final[::-1])]


def mark_monsters(final, expression, regex):
    size = len(final)
    vector = ''.join(final)
    match = regex.search(vector)
    while match is not None:
        start = match.start()
        for i, value in enumerate(expression):
            if value == "#":
                vector = vector[:start + i] + "O" + vector[start + i + 1:]
        match = regex.search(vector)

    return [vector[i:i+size] for i in range(0, size*size, size)]


def main():
    data = read_data()
    tiles = conver_data_to_tiles(data)

    tiles_base = determine_tile_types(tiles)

    grid = create_base_map(tiles_base)
    final = []
    for row in grid:
        for i in range(1, 9):
            line = ""
            for item in row:
                line += item['rows'][i][1:-1]
            final.append(line)

    size = len(final)
    expression = SEA_MONSTER.replace('\n', '.' * (size - 20)).replace(' ', '.')

    regex = re.compile(expression)

    final = find_final_orientation(final, regex)
    final = mark_monsters(final, expression, regex)
    print(len(final))
    print(len(final[0]))
    print('\n'.join(final))
    print(''.join(final).count("#"))


if __name__ == '__main__':
    main()
