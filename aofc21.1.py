from __future__ import print_function
import sys
from copy import copy


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def create_set_pairs_in_list(data):
    results = []
    every_ingredient = []
    for row in data:
        split1 = row.split(" (")
        ingredients = set(split1[0].split(" "))
        every_ingredient.extend(split1[0].split(" "))
        allergens = set(split1[1].strip(")").replace(
            "contains ", "").split(", "))
        results.append([ingredients, allergens])

    return every_ingredient, results


def main():
    every_ingredient, data = create_set_pairs_in_list(read_data())

    all_allergens = set()
    found_allergens = set()
    allergen_ingredients = set()
    all_ingredients = set()
    alergen_dict = {}

    for item in data:
        all_allergens.update(item[1])
        all_ingredients.update(item[0])

    while len(all_allergens) > len(allergen_ingredients):
        for allergen in all_allergens - found_allergens:
            possible_ingredients = copy(all_ingredients) - allergen_ingredients
            for item in data:
                if allergen in item[1]:
                    possible_ingredients = possible_ingredients & item[0]
            if len(possible_ingredients) == 1:
                found_allergens.add(allergen)
                allergen_ingredients.update(possible_ingredients)
                alergen_dict[allergen] = possible_ingredients.pop()

    print("found: " + str(found_allergens))
    print("allergen_ingredients: " + str(allergen_ingredients))

    clean_ingredients = all_ingredients - allergen_ingredients
    print("clean_ingredients: " + str(clean_ingredients))
    total_clean = 0

    for ingredient in clean_ingredients:
        x = every_ingredient.count(ingredient)
        if x > 1:
            print(x)
        total_clean += x
    print("total clean: " + str(total_clean))

    sorted_alergens = []
    for key in sorted(alergen_dict.keys()):
        sorted_alergens.append(alergen_dict[key])

    print("Sorted Alergens: " + ','.join(sorted_alergens))


if __name__ == '__main__':
    main()
