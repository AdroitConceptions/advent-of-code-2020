from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def create_decks(data):
    player_1 = []
    player_2 = []

    player = None
    for item in data:
        if 'Player' in item:
            player = item.split()[1].strip(":")
            continue
        if item == '':
            continue
        if player == '1':
            player_1.append(int(item))
        if player == '2':
            player_2.append(int(item))

    return player_1, player_2


def main():
    player_1, player_2 = create_decks(read_data())

    round_count = 0
    player_1_wins = 0
    player_2_wins = 0
    while len(player_1) != 0 and len(player_2) != 0:
        round_count += 1

        p1 = player_1.pop(0)
        p2 = player_2.pop(0)

        if p1 > p2:
            player_1_wins += 1
            player_1.append(p1)
            player_1.append(p2)
        if p1 < p2:
            player_2_wins += 1
            player_2.append(p2)
            player_2.append(p1)

    print("Rounds: {}, Player 1 Wins: {}, Player 2 Wins: {}".format(
        round_count,
        player_1_wins,
        player_2_wins
    ))

    winner_order = player_1 + player_2

    score = 0
    for pair in zip(winner_order, range(len(winner_order), 0, -1)):
        score += pair[0] * pair[1]
    print(score)


if __name__ == '__main__':
    main()
