from __future__ import print_function
import sys
from copy import copy


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def create_decks(data):
    player_1 = []
    player_2 = []

    player = None
    for item in data:
        if 'Player' in item:
            player = item.split()[1].strip(":")
            continue
        if item == '':
            continue
        if player == '1':
            player_1.append(int(item))
        if player == '2':
            player_2.append(int(item))

    return player_1, player_2


def play_game(player_1, player_2):
    hand_sets = set()

    while len(player_1) != 0 and len(player_2) != 0:
        hand_format = (tuple(player_1), tuple(player_2))

        if hand_format in hand_sets:
            return [1], []
        hand_sets.add(hand_format)

        p1 = player_1.pop(0)
        p2 = player_2.pop(0)

        winner = None
        if len(player_1) >= p1 and len(player_2) >= p2:
            a, b = play_game(
                copy(player_1[:p1]),
                copy(player_2[:p2])
            )
            if len(a) > len(b):
                winner = 1
            if len(b) > len(a):
                winner = 2
        else:
            if p1 > p2:
                winner = 1
            if p2 > p1:
                winner = 2

        if winner == 1:
            player_1.append(p1)
            player_1.append(p2)
        if winner == 2:
            player_2.append(p2)
            player_2.append(p1)
        if winner not in [1, 2]:
            raise Exception("bad winner")

    return player_1, player_2


def main():
    player_1, player_2 = create_decks(read_data())

    player_1, player_2 = play_game(player_1, player_2)

    winner_order = player_1 + player_2

    score = 0
    for pair in zip(winner_order, range(len(winner_order), 0, -1)):
        score += pair[0] * pair[1]
    print(score)


if __name__ == '__main__':
    main()
