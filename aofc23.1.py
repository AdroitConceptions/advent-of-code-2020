from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def step(data):
    lift = data[1:4]
    del data[1:4]
    # print(lift)
    target = data[0] - 1
    while target not in data:
        if target == 0:
            target = 10
        target -= 1
    insert_location = data.index(target)
    data = data[:insert_location+1] + lift + data[insert_location+1:]
    data = data[1:] + [data[0]]

    return data


def main():
    data = [int(x) for x in read_data()[0]]
    for _ in range(100):
        data = step(data)
        # print(data)

    while data[0] != 1:
        data = data[1:] + [data[0]]
    print("".join([str(x) for x in data[1:]]))


if __name__ == '__main__':
    main()
