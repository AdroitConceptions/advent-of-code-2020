from __future__ import print_function
import sys


class node (object):
    def __init__(self, value):
        self.value = value
        self.next = None


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def step(lookup, current_cup):
    remove_root = current_cup.next
    current_cup.next = remove_root.next.next.next
    target = current_cup.value - 1
    if target == 0:
        target = 1000000
    removed_vals = [remove_root.value,
                    remove_root.next.value, remove_root.next.next.value]
    while target in removed_vals:
        target -= 1
        if target == 0:
            target = 1000000

    target_root = lookup[target]
    remove_root.next.next.next = target_root.next
    target_root.next = remove_root


def main():
    data = [int(x) for x in read_data()[0]]
    data.extend(range(10, 1000001))
    # print(data)
    lookup = {}

    root_node = node(data[0])
    last_node = root_node
    lookup[data[0]] = root_node
    for item in data[1:]:
        this_node = node(item)
        lookup[item] = this_node
        this_node.next = root_node
        last_node.next = this_node
        last_node = this_node

    current_cup = root_node
    for i in range(10000000):
        # if i % 1000 == 0:
        #    print(i)
        step(lookup, current_cup)
        current_cup = current_cup.next

    node_1 = lookup[1]
    print(node_1.next.value, node_1.next.next.value)
    print(node_1.next.value * node_1.next.next.value)


if __name__ == '__main__':
    main()
