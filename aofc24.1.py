from __future__ import print_function
import sys

DIRECTIONS = {
    'e': (1, 0),
    'se': (0, 1),
    'sw': (-1, 1),
    'w': (-1, 0),
    'nw': (0, -1),
    'ne': (1, -1,)
}


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def main():
    tiles = {}
    data = read_data()
    for row in data:
        tile_position = [0, 0]
        while len(row) > 0:
            if row[0] in ['n', 's']:
                step_offset = DIRECTIONS[row[:2]]
                row = row[2:]
            else:
                step_offset = DIRECTIONS[row[:1]]
                row = row[1:]
            tile_position[0] += step_offset[0]
            tile_position[1] += step_offset[1]
        tile_position = tuple(tile_position)
        tiles[tile_position] = not tiles.get(tile_position, False)

    black = 0
    white = 0
    for item in tiles.values():
        if item:
            black += 1
        else:
            white += 1

    print("Black: {}\nWhite: {}".format(black, white))


if __name__ == '__main__':
    main()
