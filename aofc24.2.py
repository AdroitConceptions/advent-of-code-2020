from __future__ import print_function
import sys

DIRECTIONS = {
    'e': (1, 0),
    'se': (0, 1),
    'sw': (-1, 1),
    'w': (-1, 0),
    'nw': (0, -1),
    'ne': (1, -1,)
}


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def day_zero(data):
    tiles = {}
    for row in data:
        tile_position = [0, 0]
        while len(row) > 0:
            if row[0] in ['n', 's']:
                step_offset = DIRECTIONS[row[:2]]
                row = row[2:]
            else:
                step_offset = DIRECTIONS[row[:1]]
                row = row[1:]
            tile_position[0] += step_offset[0]
            tile_position[1] += step_offset[1]
        tile_position = tuple(tile_position)
        tiles[tile_position] = not tiles.get(tile_position, False)

    # sparse array with only the 'blacks'
    for key, value in tiles.items():
        if not value:
            tiles.pop(key)

    return tiles


def adjacent_keys(key):
    return [(key[0] + x[0], key[1] + x[1]) for x in DIRECTIONS.values()]


def adjacent_count(key, data):
    cnt = 0
    for akey in adjacent_keys(key):
        if data.get(akey, False):
            cnt += 1
    return cnt


def step(data):
    next_data = {}
    white_checks = set()

    for key in data:
        ac = adjacent_count(key, data)
        if ac == 1:
            next_data[key] = True
        white_checks.update(adjacent_keys(key))

    for key in white_checks:
        ac = adjacent_count(key, data)
        if ac == 2:
            next_data[key] = True

    return next_data


def main():
    data = read_data()
    tiles = day_zero(data)

    for day in range(100):
        tiles = step(tiles)
    black = 0
    white = 0
    for item in tiles.values():
        if item:
            black += 1
        else:
            white += 1

    print("Black: {}\nWhite: {}".format(black, white))


if __name__ == '__main__':
    main()
