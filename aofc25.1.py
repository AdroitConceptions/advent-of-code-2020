from __future__ import print_function
import sys
from sympy.ntheory.residue_ntheory import discrete_log


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def create_hash(base, loop):
    hash = 1

    for _ in range(loop):
        hash *= base
        hash %= 20201227

    return hash


def determine_loop_size(value):
    loop = 1
    while create_hash(7, loop) != value:
        loop += 1

    return loop


def main():
    data = read_data()
    pks = [int(x) for x in data]
    print(
        f"Part #1: {pow(pks[1], discrete_log(20201227, pks[0], 7), 20201227)}")


if __name__ == '__main__':
    main()
