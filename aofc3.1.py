from __future__ import print_function
import sys


def main():
    rows = []
    for line in sys.stdin:
        rows.append([x == '#' for x in line.strip()])

    trees = 0
    mod_lenth = len(rows[0])
    index = 0
    for row in rows:
        if(row[index]):
            trees += 1
        index += 3
        index %= mod_lenth

    print(trees)


if __name__ == '__main__':
    main()
