from __future__ import print_function
import sys
import math

MOTION = [
    [1, 1],
    [3, 1],
    [5, 1],
    [7, 1],
    [1, 2],
]


def main():
    rows = []
    for line in sys.stdin:
        rows.append([x == '#' for x in line.strip()])

    collisions = []
    mod_lenth = len(rows[0])

    for slope in MOTION:
        trees = 0
        index = 0
        for row in rows[::slope[1]]:
            if(row[index]):
                trees += 1
            index += slope[0]
            index %= mod_lenth
        print(trees)
        collisions.append(trees)

    print(math.prod(collisions))


if __name__ == '__main__':
    main()
