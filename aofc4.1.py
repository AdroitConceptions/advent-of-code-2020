from __future__ import print_function
import sys

REQUIRED_KEYS = [
    'ecl',
    'pid',
    'eyr',
    'hcl',
    'byr',
    'iyr',
    # 'cid',
    'hgt',
]

VALID_EYE_COLORS = [
    'amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth',
]

VALID_HEX = "0123456789abcdef"


def validate_number_range(value, min_val, max_val):
    return min_val <= value and value <= max_val


def validate_hgt(value):
    last_2 = value[-2:]
    if last_2 == 'cm':
        return validate_number_range(int(value[:-2]), 150, 193)
    if last_2 == 'in':
        return validate_number_range(int(value[:-2]), 59, 76)
    return False


def validate_hcl(value):
    return value[0] == '#' and len(value) == 7 and all([x in VALID_HEX for x in value[1:]])


def validate_ecl(value):
    return value in VALID_EYE_COLORS


def validate_pid(value):
    try:
        int(value)
        return len(value) == 9
    except ValueError:
        return False


def validate(value):
    if not all(x in value for x in REQUIRED_KEYS):
        return False

    # for 4.1 just return true at this point, this is the 4.2 additions
    return all([
        validate_number_range(int(value['byr']), 1920, 2002),
        validate_number_range(int(value['iyr']), 2010, 2020),
        validate_number_range(int(value['eyr']), 2020, 2030),
        validate_hgt(value['hgt']),
        validate_hcl(value['hcl']),
        validate_ecl(value['ecl']),
        validate_pid(value['pid']),
    ])


def read_data():
    data = []
    item = {}
    for line in sys.stdin:
        if len(line.strip()) == 0:
            data.append(item)
            item = {}
        split = line.split()

        for x in split:
            y = x.strip().split(":")
            item[y[0]] = y[1]

    data.append(item)
    return data


def main():
    data = read_data()

    valid = 0
    for item in data:
        valid += 1 if validate(item) else 0

    print(valid)


if __name__ == '__main__':
    main()
