from __future__ import print_function
import sys


def read_data():
    data = []
    for line in sys.stdin:
        data.append(parse(line))

    return data


def parse(value):
    result = 0
    for x in value.strip():
        result *= 2
        if x in 'BR':
            result += 1
    return result


def main():
    data = read_data()

    data.sort()

    # 5.1 just print max(data)

    data_min = min(data)
    data_max = max(data)

    for x in range(data_min, data_max):
        if x not in data:
            print(x)


if __name__ == '__main__':
    main()
