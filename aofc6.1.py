from __future__ import print_function
import sys


def read_data():
    data = []
    group = set()

    for line in sys.stdin:
        if line.strip() == '':
            data.append(group)
            group = set()
        else:
            group.update(line.strip())

    data.append(group)
    return data


def main():
    data = read_data()

    print(sum([len(x) for x in data]))


if __name__ == '__main__':
    main()
