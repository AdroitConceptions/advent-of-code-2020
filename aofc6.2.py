from __future__ import print_function
import sys


def read_data():
    data = []
    group = []

    for line in sys.stdin:
        if line.strip() == '':
            data.append(group)
            group = []
        else:
            group.append(line.strip())

    data.append(group)
    return data


def all_set(data):
    group = data[0]

    for x in data[1:]:
        group = group.intersection(x)

    return group


def main():
    data = read_data()
    data = [[set(x) for x in y] for y in data]
    data = [all_set(x) for x in data]
    # print(data)
    print(sum([len(x) for x in data]))


if __name__ == '__main__':
    main()
