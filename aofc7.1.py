from __future__ import print_function
from collections import defaultdict
import sys


def main():
    contained_in = defaultdict(lambda: set())

    for line in sys.stdin:
        x = line.split(" bags contain ")
        if 'no other bags.' not in x[1]:
            y = x[1].split(',')
            for z in y:
                w = ' '.join(z.strip(" .\n").split(
                    " ", 1)[1].split(" ")[:-1])
                # print(w + " -> " + x[0])
                contained_in[w].add(x[0])

    results = set()

    found = set(["shiny gold"])
    print(found)
    while len(found) > 0:
        x = found.pop()
        for y in contained_in[x]:
            if y not in results:
                results.add(y)
                found.add(y)

    print(results)
    print(len(results))


if __name__ == '__main__':
    main()
