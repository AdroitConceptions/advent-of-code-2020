from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def parse_instruction(value):
    spl = value.split()

    return spl[0], int(spl[1])


def process_instruction(instructions, accumulator, program_counter):
    opcode, value = parse_instruction(instructions[program_counter])

    if opcode == 'acc':
        program_counter += 1
        accumulator += value
    elif opcode == 'jmp':
        program_counter += value
    else:  # nop
        program_counter += 1
    return accumulator, program_counter


def main():
    instructions = read_data()
    accumulator = 0
    program_counter = 0

    reached_instructions = set()

    while program_counter not in reached_instructions and program_counter < len(instructions):
        reached_instructions.add(program_counter)

        accumulator, program_counter = process_instruction(
            instructions, accumulator, program_counter)

    print(accumulator)


if __name__ == '__main__':
    main()
