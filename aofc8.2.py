from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def parse_instruction(value):
    spl = value.split()

    return [spl[0], int(spl[1])]


def process_instruction(instruction, accumulator, program_counter):
    opcode, value = instruction

    if opcode == 'acc':
        program_counter += 1
        accumulator += value
    elif opcode == 'jmp':
        program_counter += value
    else:  # nop
        program_counter += 1
    return accumulator, program_counter


def execute_instructions(instructions):
    accumulator = 0
    program_counter = 0
    reached_instructions = set()
    while program_counter not in reached_instructions and program_counter < len(instructions):
        reached_instructions.add(program_counter)

        accumulator, program_counter = process_instruction(
            instructions[program_counter], accumulator, program_counter)

    return accumulator, program_counter >= len(instructions)


def main():
    instructions = read_data()
    instructions = [parse_instruction(x) for x in instructions]

    for x in range(len(instructions)):
        if instructions[x][0] in ['jmp', 'nop']:
            old_instruction = instructions[x][0]
            instructions[x][0] = 'nop' if old_instruction == 'jmp' else 'jmp'

            accumulator, valid_exit = execute_instructions(instructions)
            print(accumulator, valid_exit)

            if valid_exit:
                break
            instructions[x][0] = old_instruction


if __name__ == '__main__':
    main()
