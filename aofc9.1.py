from __future__ import print_function
import sys

PREABLE_SIZE = 25


def read_data():
    data = []

    for line in sys.stdin:
        data.append(int(line.strip()))

    return data


def main():
    data = read_data()

    for i in range(PREABLE_SIZE, len(data)):
        valid = False
        for a in range(i-PREABLE_SIZE, i-1):
            for b in range(i-PREABLE_SIZE + 1, i):
                if data[i] == data[a] + data[b]:
                    valid = True
        if not valid:
            print(data[i])
            break


if __name__ == '__main__':
    main()
