from __future__ import print_function
import sys

PREABLE_SIZE = 25
ERROR_NUMBER = 15353384


def read_data():
    data = []

    for line in sys.stdin:
        data.append(int(line.strip()))

    return data


def main():
    data = read_data()

    for i in range(PREABLE_SIZE, len(data) - 1):
        for j in range(i+1, len(data)):
            if sum(data[i:j]) == ERROR_NUMBER:
                print(min(data[i:j]) + max(data[i:j]))
                return


if __name__ == '__main__':
    main()
