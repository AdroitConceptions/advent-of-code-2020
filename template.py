from __future__ import print_function
import sys


def read_data():
    data = []

    for line in sys.stdin:
        data.append(line.strip())

    return data


def main():
    data = read_data()

    print(data)


if __name__ == '__main__':
    main()
